## Investigation of Pseudomonas putida KT2440 transcriptome in different carbon sources reveals novelty in bacterial uptake systems.

#### Isotta D’Arrigo, João G. R. Cardoso, Maja Rennig, Nikolaus Sonnenschein, Markus J. Herrgård, Katherine S. Long

This repository contains data, scipts and notebooks for the paper.

<img src="https://gitlab.com/joaocardoso/ppu-rna-seq-2016/raw/master/ppu.png"/>

The final version of the notebooks can be viewed:

1. [model corrections](http://nbviewer.jupyter.org/urls/gitlab.com/joaocardoso/ppu-rna-seq-2016/raw/master/notebooks/1-model%20corrections.ipynb)
2. [add ferulic acid degradation pathway](http://nbviewer.jupyter.org/urls/gitlab.com/joaocardoso/ppu-rna-seq-2016/raw/master/notebooks/2-add%20ferulic%20acid%20degradation%20pathway.ipynb)
3. [add the tricarboxylic acid transport](http://nbviewer.jupyter.org/urls/gitlab.com/joaocardoso/ppu-rna-seq-2016/raw/master/notebooks/3-add%20the%20tricarboxylic%20acid%20transport.ipynb)
4. [simulate growth in sole carbon sources](http://nbviewer.jupyter.org/urls/gitlab.com/joaocardoso/ppu-rna-seq-2016/raw/master/notebooks/4-simulate%20growth%20in%20sole%20carbon%20sources.ipynb)
5. [analyse the rna-seq data](http://nbviewer.jupyter.org/urls/gitlab.com/joaocardoso/ppu-rna-seq-2016/raw/master/notebooks/5-analyse%20the%20rna-seq%20data.ipynb)
