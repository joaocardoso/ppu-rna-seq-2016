import sys

from cameo import load_model, Reaction
from cobra.io.json import save_json_model

model = load_model(sys.argv[1])

# http://beta.pseudomonas.com/feature/show?id=1485003
if "PP_1843" not in model.genes:
    transport_mg = Reaction("corA_Mg2")
    transport_mg.name = "corA transport Mg2+"
    transport_mg.add_metabolites({model.metabolites.mg2_e: -1, model.metabolites.mg2_c: 1})
    transport_mg.gene_reaction_rule = "PP_1843"
    transport_mg.lower_bound = -1000
    
    transport_co = Reaction("corA_Co")
    transport_co.name = "corA transport Co2+"
    transport_co.add_metabolites({model.metabolites.cobalt2_e: -1, model.metabolites.cobalt2_c: 1})
    transport_co.gene_reaction_rule = "PP_1843"
    transport_co.lower_bound = -1000

    model.add_reactions([transport_co, transport_mg])
    
    save_json_model(model, sys.argv[1])
