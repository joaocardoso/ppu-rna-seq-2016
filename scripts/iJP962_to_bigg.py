from cameo import models
from cobra.io.json import save_json_model
from cameo import Reaction, Metabolite
from cameo.core.solver_based_model import to_solver_based_model

iJP962 = models.minho.iJP962

reaction2bigg = {}
with open("simcodes-new.csv", "r") as reaction_codes:
    for line in reaction_codes:
        splited = line.replace("\n", "").split("\t")
        if len(splited) > 1:
            for v in splited[1:]:
                if len(v) > 0:
                    reaction2bigg[int(v)] = splited[0]


metabolite2bigg = {}
with open("mcodes-new.csv", "r") as metabolite_codes:
    for line in metabolite_codes:
        splited = line.replace("\n", "").split("\t")
        if len(splited) > 1:
            for v in splited[1:]:
                if len(v) > 0:
                     metabolite2bigg[int(v)] = splited[0]


for r in iJP962.reactions:
    if r.id.startswith("EX"):
        mid = r.id[3:]
        r.name = iJP962.metabolites.get_by_id(mid).name  + " exchange" 
        if mid.startswith("EC"):
             mid = metabolite2bigg.get(int(mid.replace("EC", "")), mid) + "_e"
        else:
             mid = metabolite2bigg.get(int(mid.replace("C", "")), mid) + "_c"
        r.id = "EX_%s" % mid        
    else:
        id = r.id.replace("RR", "").replace("IR", "")
        r.id = reaction2bigg.get(int(id), r.id) 
  

for m in iJP962.metabolites:
    if m.id.startswith("EC"):
        m.id = metabolite2bigg.get(int(m.id.replace("EC", "")), m.id) + "_e"
    else:
        m.id = metabolite2bigg.get(int(m.id.replace("C", "")), m.id) + "_c"

        
iJP962.repair()
iJP962 = to_solver_based_model(iJP962)
                
save_json_model(iJP962, "iJP962_bigg.json")