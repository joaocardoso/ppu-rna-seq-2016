import sys

from cameo import load_model, Metabolite, Reaction
from cobra.io.json import save_json_model

model = load_model(sys.argv[1])

#cit_e + ca2_e + h_e <=> h_c + ca2_c + cit_c

cit_e = model.metabolites.cit_e
ca2_e = model.metabolites.ca2_e
h_e =model.metabolites.h_e
h_c =model.metabolites.h_c
ca2_c =model.metabolites.ca2_c
cit_c =model.metabolites.cit_c

CITt14 = Reaction("CITt14")
CITt14.name = "Citrate transport in via Ca complex"
CITt14.gene_reaction_rule = "PP_0147"
CITt14.add_metabolites({
        cit_e: -1,
        ca2_e: -1,
        h_e: -1,
        h_c: 1,
        ca2_c: 1,
        cit_c: 1
})

CITt14.lower_bound = -1000

model.add_reaction(CITt14)

save_json_model(model, sys.argv[1])
