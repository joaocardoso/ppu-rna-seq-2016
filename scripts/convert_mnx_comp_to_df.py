import sys
import gzip
from pandas import DataFrame

xref = DataFrame(columns=["database", "id", "mnx_id", "name"])
i = 0
with gzip.open("data/raw/metanetx_comp_xref.gz") as mnx_ref_file:
	for line in mnx_ref_file:
		line = line.decode("utf-8")
		if line.startswith("#"): continue

		split = line.strip("\n").split("\t")
		db, ref = split[0].split(":", 1)
		names = split[2].split("|")
		xref.loc[i] = [db, ref, split[1], tuple(names)]
		i += 1

xref.to_csv("data/aux/mnx_comp_df.tsv.gz", sep="\t", compression="gzip")