import sys
import gzip
from pandas import DataFrame

xref = DataFrame(columns=["database", "id", "mnx_id"])
i = 0
with gzip.open("data/raw/metanetx_reac_xref_bigg.gz") as mnx_ref_file:
	for line in mnx_ref_file:
		line = line.decode("utf-8")
		if line.startswith("#"): continue

		split = line.strip("\n").split("\t")
		db, ref = split[0].split(":", 1)
		xref.loc[i] = [db, ref, split[1]]
		i += 1

xref.to_csv("data/aux/mnx_reac_df.tsv.gz", sep="\t", compression="gzip")

