import sys

from cameo import load_model, Metabolite, Reaction
from cobra.io.json import save_json_model

model = load_model(sys.argv[1])

ca2_e = Metabolite("ca2_e")
ca2_c = Metabolite("ca2_c")

r = Reaction("EX_ca2_e")
r.add_metabolites({ca2_e: -1})
r.lower_bound = -1000

model.add_reaction(r)
model.add_metabolites([ca2_c])

save_json_model(model, sys.argv[1])
