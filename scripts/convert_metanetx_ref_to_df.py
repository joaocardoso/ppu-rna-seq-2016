import sys

from pandas import DataFrame

xref = DataFrame(columns=["database", "id", "mnx_id"])
i = 0
with open("data/raw/metanetx_reac_xref") as mnx_ref_file:
	for line in mnx_ref_file:
		if line.startswith("#"): continue

		split = line.split("\t")
		db, ref = split[0].split(":", 1)
		xref.loc[i] = [db, ref, split[1], ""]
		i += 1

xref.to_csv("data/raw/mnx_reac_df.tsv", sep="\t")

