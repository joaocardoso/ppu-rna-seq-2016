from cobra.io.json import save_json_model
from cameo import load_model
from cameo.data import metanetx
from pandas import DataFrame

model = load_model("data/models/belda_et_al_2016.sbml")

del model.compartments["BOUNDARY"]

compartments = DataFrame.from_csv("data/aux/mnx_comp_df.tsv.gz", sep="\t")
compartments['name'] = compartments['name'].apply(eval)
compartments = compartments[compartments.database == 'bigg']

chemicals = DataFrame.from_csv("data/aux/mnx_chem_df.tsv.gz", sep="\t")

reactions = DataFrame.from_csv("data/aux/mnx_reac_df.tsv.gz", sep="\t")
reactions = reactions[reactions.id.apply(lambda id: not id.startswith("EX_"))]

compartment_translation = {}

for compartment in dict(model.compartments): 
	query = compartments.query("mnx_id == @compartment")
	bigg_id = query.values[0, 1]
	names = query.values[0, 3]
	compartment_translation[compartment] = bigg_id
	model.compartments[bigg_id] = names[0]
	del model.compartments[compartment]


for metabolite in model.metabolites:
	mnx_id = metabolite.notes["COMPOUND ID"][0]
	query = chemicals.query("mnx_id == @mnx_id")
	if len(query) > 0:
		bigg_id = query.id.values[0]
		name = query.name.values[0]
		metabolite.id = bigg_id + "_" + compartment_translation[metabolite.compartment] 
		metabolite.name = name
	else:
		metabolite.id = mnx_id + "_" + compartment_translation[metabolite.compartment]

	metabolite.compartment = compartment_translation[metabolite.compartment]

for reaction in model.reactions:
	try:
		mnx_id = reaction.notes["REACTION"][0]
		query = reactions.query("mnx_id == @mnx_id")
		if len(query) > 0:
			bigg_id = query.id.values[0]
			if bigg_id.startswith("EX_"):
				print(reaction, query)
			reaction.id = bigg_id
	except KeyError:
		pass
	except:
		print(query)

for reaction in model.exchanges:
	metabolite = list(reaction.metabolites.keys())[0]
	reaction_id = "EX_%s" % (metabolite.id)
	try:
		print(model.reactions.get_by_id(reaction_id))
	except KeyError:
		reaction.id = reaction_id	
	if len(metabolite.name) > 0:
		reaction.name = "Exchange %s (%s)" % (metabolite.name, model.compartments[metabolite.compartment])
	else:
		reaction.name = "Exchange %s" % metabolite.id

model.repair()
save_json_model(model, "data/models/belda_et_al_2016_bigg.json")
