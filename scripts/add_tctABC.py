import sys

import bioservices

from cameo import load_model, Metabolite, Reaction
from cobra.io.json import save_json_model
from bioservices import ChEBI


model = load_model(sys.argv[1])
na1_e = model.metabolites.na1_e
na1_c = model.metabolites.na1_c
chebi = ChEBI()
#tricarboxylic acid is "CHEBI:27093"
tca = chebi.getCompleteEntity("CHEBI:27093")

i = 1
for ontology_children in tca.OntologyChildren:
    if ontology_children.type == "is a":
        ontology_children.chebiName.endswith("ic acid")
        ontology_children.chebiName = ontology_children.chebiName.replace("ic acid", "ate").lower()
        
    metabolites = [m for m in model.metabolites if ontology_children.chebiName == m.name.lower()]
    if len(metabolites) > 0:
        met = metabolites[0]
        try:
            ext_met = model.metabolites.get_by_id(met.id.replace("_c", "_e"))
        except KeyError:
            ext_met = Metabolite(met.id.replace("_c", "_e"))
            ext_met.name = met.name + "[e]"
        
        r = Reaction("TCTt%i" % i)
        r.name = "Tricarboxylate symport (%s:Na+)" % met.name
        r.add_metabolites({ext_met: -1, na1_e: -1, met: 1, na1_c: 1})
        r.gene_reaction_rule = "PP1419 and PP1416 and PP1417 and PP1418"
        model.add_reaction(r)
        i += 1
        
save_json_model(model, sys.argv[1])